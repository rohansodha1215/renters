package main

import (
	auth2 "github.com/Rohan12152001/Renters/endpoints/auth"
	"github.com/Rohan12152001/Renters/endpoints/listing"
	"github.com/Rohan12152001/Renters/endpoints/pages"
	"github.com/Rohan12152001/Renters/endpoints/register"
	"github.com/Rohan12152001/Renters/endpoints/search"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func main() {
	server := gin.Default()
	server.LoadHTMLGlob("templates/*.html")

	auth := auth2.New()
	auth.SetRoutes(server)

	reg := register.New()
	reg.SetRoutes(server)

	page := pages.New()
	page.SetRoutes(server)

	list := listing.New()
	list.Setroutes(server)

	search := search.New()
	search.SetRoutes(server)

	server.Static("/static", "./static")

	server.Run()
}

/*
controllers: handle http calls
	- endpoints
		- login
			- login.go
				- POST /login
		- registration
			- POST /register
		- listing
			- POST /listing
		- search
			- POST /search
	- managers
		- login

*/

//const (
//	host     = "localhost"
//	port     = 5432
//	user     = "postgres"
//	password = "admin"
//	dbname   = "Renters"
//)
//
//type S struct {
//	B1 int     `db:"b1"`
//	B2 string  `db:"b2"`
//	B3 bool    `db:"b3"`
//	B4 float64 `db:"b4"`
//}

//func main() {
//	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
//		"password=%s dbname=%s sslmode=disable",
//		host, port, user, password, dbname)
//	db, err := sqlx.Connect("postgres", psqlInfo)
//	if err != nil {
//		panic(err)
//	}
//
//	b2Array := []string{
//		"b",
//		"a",
//		"cd",
//		"abcd",
//		"efgh",
//		"lmn",
//		"xyz",
//		"d",
//		"vipul",
//		"rohan",
//	}
//
//	b3Array := []bool{
//		true,
//		false,
//	}
//
//	for ind := 0; ind < 1000000; ind++ {
//		var h S
//		h.B1 = ind + 1
//		h.B2 = b2Array[ind%10]
//		h.B3 = b3Array[ind%2]
//		h.B4 = (rand.Float64() * (10000 - 1)) + 1
//		//b1 := ind + 1
//		//b2 := b2Array[ind%10]
//		//b3 := b3Array[ind%2]
//		//b4 := (rand.Float64() * (10000 - 1)) + 1
//
//		//query := fmt.Sprintf("Insert into Tmp1 (b1, b2, b3, b4) values (%d, %q, %q, %v)", b1, b2, b3, b4)
//		query := "Insert into Tmp1 (b1, b2, b3, b4) values (:b1, :b2, :b3, :b4)"
//		_, err := db.NamedExec(query, &h)
//		if err != nil {
//			fmt.Println(err)
//		}
//	}
//}
