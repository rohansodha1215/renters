package register

import (
	"encoding/json"
	"github.com/Rohan12152001/Renters/managers/user"
	"github.com/gin-gonic/gin"
	"io/ioutil"
)

type Register struct {
	usermanager user.UserManager
}

type UserRegisterPayload struct {
	Fname       string `json:"fname"`
	Lname       string `json:"lname"`
	Email       string `json:"email"`
	Phone       string `json:"phone"`
	Country     string `json:"country"`
	Password    string `json:"password"`
	Gender      string `json:"gender"`
	Description string `json:"description"`
}

func New() Register {
	return Register{
		usermanager: user.New(),
	}
}

func (R *Register) SetRoutes(engine *gin.Engine) {
	engine.POST("/register", R.RegisterCall)
}

// RegisterCall for endpoint layer
func (R *Register) RegisterCall(context *gin.Context) {
	var payload UserRegisterPayload

	bytesRecieved, err := ioutil.ReadAll(context.Request.Body)
	if err != nil {
		context.Error(err)
		return
	}

	err = json.Unmarshal(bytesRecieved, &payload)
	if err != nil {
		context.Error(err)
		return
	}

	UserId, err := R.usermanager.CreateUser(payload.Fname,
		payload.Lname,
		payload.Email,
		payload.Phone,
		payload.Country,
		payload.Password,
		payload.Gender,
		payload.Description)
	if err != nil {
		context.Error(err)
		context.Status(500)
		return
	}

	context.JSON(200, gin.H{
		"ID": UserId,
	})

}

// RegisterCall is manager
//func (R *Register) RegisterCall(context *gin.Context){
//	context.JSON(200, gin.H{
//		"message":"Register called",
//	})
//}
