package search

import (
	"github.com/Rohan12152001/Renters/managers/middleware"
	"github.com/Rohan12152001/Renters/managers/search"
	"github.com/gin-gonic/gin"
)

type Search struct {
	searchManager search.SearchManager
	authManager   middleware.AuthManager
}

func New() Search {
	return Search{
		searchManager: search.New(),
		authManager:   middleware.New(),
	}
}

func (S *Search) SetRoutes(engine *gin.Engine) {
	engine.GET("/search_property", S.authManager.AuthMiddleWareWithUser, S.Search_property)
}

func (S *Search) Search_property(context *gin.Context) {
	// by default sort by desc posted_date (This is not a column yet)

	// This is a map[string] string
	queryParams := context.Request.URL.Query()

	// Pass queryparams to manager
	response, err := S.searchManager.SearchProperty(queryParams)
	if err != nil {
		context.AbortWithStatus(500)
		return
	}
	context.JSON(200, gin.H{
		"response": response,
	})

}
