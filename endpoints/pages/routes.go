package pages

import (
	"github.com/Rohan12152001/Renters/managers/middleware"
	"github.com/gin-gonic/gin"
)

type Pages struct {
	authManager middleware.AuthManager
}

func New() Pages {
	return Pages{
		authManager: middleware.New(),
	}
}

func (P *Pages) SetRoutes(engine *gin.Engine) {
	groupRouter := engine.Group("/app")
	// groupRouter.Use(authMiddleware)
	groupRouter.GET("/home", P.Home)
	groupRouter.GET("/registerPage", P.RegisterPage)
}

func (P *Pages) Home(context *gin.Context) {

	// server html file here
	context.HTML(
		200,
		"home.html",
		gin.H{
			"message": "Hello",
		},
	)
}

func (P *Pages) RegisterPage(context *gin.Context) {

	// server html file here
	context.HTML(
		200,
		"register.html",
		nil,
	)
}
