package listing

import (
	"encoding/json"
	"fmt"
	"github.com/Rohan12152001/Renters/managers/listing"
	"github.com/Rohan12152001/Renters/managers/listingImage"
	"github.com/Rohan12152001/Renters/managers/middleware"
	"github.com/Rohan12152001/Renters/managers/user"
	"github.com/Rohan12152001/Renters/utils"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"io/ioutil"
	"os"
)

type ArrayString [][]string

type Listing struct {
	listManager         listing.ListManager
	userManager         user.UserManager
	authManager         middleware.AuthManager
	listingImageManager listingImage.ListingImageManager
}

type ListingPayload struct {
	Room_type          string   `json:"room___type"`
	Rent_amount        int      `json:"rent___amount"`
	Rent_currency      string   `json:"rent___currency"`
	Lease_months       int      `json:"lease___months"`
	Deposit            int      `json:"deposit"`
	Gender_environment string   `json:"gender___environment"`
	Property_type      string   `json:"property___type"`
	Cooking_allowed    bool     `json:"cooking___allowed"`
	Nationality        string   `json:"nationality"`
	Rent_negotiable    bool     `json:"rent___negotiable"`
	Owner_staying      bool     `json:"owner___staying"`
	Pet_allowed        bool     `json:"pet___allowed"`
	Floor              string   `json:"floor"`
	Availability_date  string   `json:"availability___date"` // Doubtful !
	Square_feet        float64  `json:"square___feet"`
	Pincode            int      `json:"pincode"`
	City               string   `json:"city"`
	Country            string   `json:"country"`
	ImageArray         []string `json:"image_array"`
}

func New() Listing {
	return Listing{
		listManager:         listing.New(),
		userManager:         user.New(),
		authManager:         middleware.New(),
		listingImageManager: listingImage.New(),
	}
}

func (l *Listing) Setroutes(engine *gin.Engine) {
	engine.POST("/create_room", l.authManager.AuthMiddleWareWithUser, l.CreateRoom)
	engine.POST("/upload_image", l.authManager.AuthMiddleWareWithUser, l.Upload_Image)
}

// Create room in listingDB and listingImageDB
func (l *Listing) CreateRoom(ginContext *gin.Context) {
	userObject, ok := utils.GetUserFromContext(ginContext)
	if !ok {
		ginContext.AbortWithStatus(500)
		return
	}

	var listPayload ListingPayload
	b, err := ioutil.ReadAll(ginContext.Request.Body)
	//tmp := strings.NewReader(ginContext.Request.FormValue("body"))
	//b, err := ioutil.ReadAll(tmp)
	//if err != nil {
	//	ginContext.AbortWithStatus(500)
	//	ginContext.Error(err)
	//	return
	//}

	err = json.Unmarshal(b, &listPayload)
	if err != nil {
		ginContext.AbortWithStatus(500)
		ginContext.Error(err)
		return
	}

	// we got the listPayload, now call manager & insert data into table
	id, err := l.listManager.CreateRoom(
		ginContext,
		userObject.Id,
		listPayload.Room_type,
		listPayload.Rent_amount,
		listPayload.Rent_currency,
		listPayload.Lease_months,
		listPayload.Deposit,
		listPayload.Gender_environment,
		listPayload.Property_type,
		listPayload.Cooking_allowed,
		listPayload.Nationality,
		listPayload.Rent_negotiable,
		listPayload.Owner_staying,
		listPayload.Pet_allowed,
		listPayload.Floor,
		listPayload.Availability_date,
		listPayload.Square_feet,
		listPayload.Pincode,
		listPayload.City,
		listPayload.Country)

	if err != nil {
		ginContext.AbortWithStatus(500)
		ginContext.Error(err)
		return
	}

	// Imag path prefix
	ImagePathPrefix, err := os.Getwd()
	if err != nil {
		ginContext.AbortWithStatus(500)
		ginContext.Error(err)
		return
	}
	ImagePathPrefix = ImagePathPrefix + "\\static"

	// First iterate over all images & form the [id, imageName] array here
	var imageArray ArrayString

	// Get the imageArray from the json payload
	imagesNameArray := listPayload.ImageArray

	// Form the final imageArray to pass to manager
	for _, name := range imagesNameArray {
		var tmp = []string{id, name}
		imageArray = append(imageArray, tmp)
	}

	// call manager to insert the rows
	err = l.listingImageManager.UploadRoomImage(imageArray)
	if err != nil {
		ginContext.AbortWithStatus(500)
		ginContext.Error(err)
		return
	}

	// retrieve the path of image from folder & print the path
	//fullPath, err := os.Getwd()
	//if err != nil {
	//	ginContext.AbortWithStatus(500)
	//	ginContext.Error(err)
	//	return
	//}
	//fullPath = fullPath + "\\" + path
	//fmt.Println(fullPath)

	// return Json
	ginContext.JSON(201, gin.H{
		"Id": id,
	})
}

// Only upload to the bucket
//& generate UUID
//& set context
func (l *Listing) Upload_Image(ginContext *gin.Context) {
	// First iterate over all images & form the [id, imageName] array here
	var imageArray []string

	ginContext.Request.ParseMultipartForm(10000000)
	for key, _ := range ginContext.Request.MultipartForm.File {
		// get the image from request context & store in a folder
		file, handler, err := ginContext.Request.FormFile(key)
		if err != nil {
			ginContext.AbortWithStatus(500)
			ginContext.Error(err)
			return
		}
		defer file.Close()
		fmt.Println("Size is: ", handler.Size)

		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			ginContext.AbortWithStatus(500)
			ginContext.Error(err)
			return
		}

		// write new file into folder & put the bytes
		new_UUID := uuid.New().String()
		path := fmt.Sprintf("static\\%s.png", new_UUID)

		//err = ioutil.WriteFile("static/image1.png", fileBytes, 0644)

		err = ioutil.WriteFile(path, fileBytes, 0644)
		if err != nil {
			ginContext.AbortWithStatus(500)
			ginContext.Error(err)
			return
		}

		// append the imageName to array
		tmpString := fmt.Sprintf("%s.png", new_UUID)
		imageArray = append(imageArray, tmpString)
		fmt.Println("Success upload !")
	}

	// call setContext
	ginContext.JSON(200, gin.H{
		"imageArray": imageArray,
	})
}
