package search

import (
	db "github.com/Rohan12152001/Renters/managers/search/db"
)

type manager struct {
	searchDb db.SearchDB
}

func New() SearchManager {
	return manager{
		searchDb: db.New(),
	}
}

func (m manager) SearchProperty(queryParams map[string][]string) (string, error) {
	// pass query to db layer
	// expect a response in as array of maps
	err := m.searchDb.SearchProperty(queryParams)
	if err != nil {
		return "", err
	}
	return "", nil

}
