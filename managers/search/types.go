package search

type SearchManager interface {
	SearchProperty(queryParams map[string][]string) (string, error)
}
