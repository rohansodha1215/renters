package db

type SearchDB interface {
	SearchProperty(queryParams map[string][]string) error // This must return json, error
}

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "admin"
	dbname   = "Renters"
)
