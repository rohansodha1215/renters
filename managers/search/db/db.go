package db

import (
	"encoding/json"
	_ "encoding/json"
	"fmt"
	"github.com/Rohan12152001/Renters/managers/search/data"
	_ "github.com/Rohan12152001/Renters/managers/search/data"
	"github.com/jmoiron/sqlx"
	_ "reflect"
)

var Hmap = map[string]map[string]int{
	"room_type": {
		"Master": 1,
		"Common": 1,
	},
	"rent_currency": {
		"Sgd": 1,
	},
	"gender_environment": {
		"Male":   1,
		"Female": 1,
		"All":    1,
	},
	"property_type": {
		"Condo": 1,
		"Hdb":   1,
	},
	"cooking_allowed": {
		"true":  1,
		"false": 1,
	},
	"nationality": {
		"Singapore": 1,
		"All":       1,
	},
	"rent_negotiable": {
		"true":  1,
		"false": 1,
	},
	"owner_staying": {
		"true":  1,
		"false": 1,
	},
	"pet_allowed": {
		"true":  1,
		"false": 1,
	},
	"floor": {
		"Low":  1,
		"Mid":  1,
		"High": 1,
	},
	"availability_date": { // doubt
		"All": 1,
	},
	"pincode": { // doubt
		"All": 1,
	},
	"city": {
		"Singapore": 1,
	},
	"country": {
		"Singapore": 1,
	},
}

type dbClient struct {
	db *sqlx.DB
}

type Params struct {
	Room_type          []string `json:"room_type"` // fixed
	Min_rent_amount    []string `json:"min_rent_amount"`
	Max_rent_amount    []string `json:"max_rent_amount"`
	Rent_currency      []string `json:"rent_currency"` // fixed
	Min_lease_months   []string `json:"min_lease_months"`
	Max_lease_months   []string `json:"max_lease_months"`
	Min_deposit        []string `json:"min_deposit"`
	Max_deposit        []string `json:"max_deposit"`
	Gender_environment []string `json:"gender_environment"` // fixed
	Property_type      []string `json:"property_type"`      // fixed
	Cooking_allowed    []string `json:"cooking_allowed"`    // fixed
	Nationality        []string `json:"nationality"`        // fixed
	Rent_negotiable    []string `json:"rent_negotiable"`    // fixed
	Owner_staying      []string `json:"owner_staying"`      // fixed
	Pet_allowed        []string `json:"pet_allowed"`        // fixed
	Floor              []string `json:"floor"`              // fixed
	Availability_date  []string `json:"availability_date"`  // fixed // doubtful type string OR date ?
	Min_square_feet    []string `json:"min_square_feet"`    // doubtful float64
	Max_square_feet    []string `json:"max_square_feet"`    // doubtful float64
	Pincode            []string `json:"pincode"`            // pincode is free text ?
	City               []string `json:"city"`               // fixed
	Country            []string `json:"country"`            // fixed
	District           []string `json:"district"`           // fixed	// cannot validate yet
	Street_name        []string `json:"street_name"`        // fixed // cannot validate yet
	Block              []string `json:"block"`              // fixed // cannot validate yet
	Posted_date        []string `json:"posted_date"`
	Posted_time        []string `json:"posted_time"`
	OrderBy            []string `json:"orderBy"`
	SortBy             []string `json:"sortBy"`
	Limit              []string `json:"limit"`
}

func New() SearchDB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sqlx.Connect("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	return dbClient{
		db: db,
	}
}

func (d dbClient) SearchProperty(queryParams map[string][]string) error {
	// now using queryparams form the sql query !
	min_max_queryParams_map := map[string][]string{"min_rent_amount": {"rent_amount", ">="},
		"max_rent_amount":  {"rent_amount", "<="},
		"min_lease_months": {"lease_months", ">="},
		"max_lease_months": {"lease_months", "<="},
		"min_deposit":      {"deposit", ">="},
		"max_deposit":      {"deposit", "<="},
		"min_square_feet":  {"square_feet", ">="},
		"max_square_feet":  {"square_feet", "<="}}

	// form struct for queryParams
	var structuredParam Params
	b, err := json.Marshal(queryParams)
	if err != nil {
		fmt.Println(err)
		return err
	}

	err = json.Unmarshal(b, &structuredParam)
	if err != nil {
		fmt.Println(err)
		return err
	}

	// Now check the whole struct & validate
	//v := reflect.ValueOf(structuredParam)
	//typeOfS := v.Type()
	//
	//for i := 0; i < v.NumField(); i++ {
	//	if _, ok := min_max_queryParams_map[typeOfS.Field(i).Name]; ok || typeOfS.Field(i).Name == "Pincode" || typeOfS.Field(i).Name == "Availability_date" || typeOfS.Field(i).Name == "District" || typeOfS.Field(i).Name == "Street_name" || typeOfS.Field(i).Name == "Block" {
	//		continue
	//	}
	//	temp, ok := v.Field(i).Interface().([]string)
	//	// Take care of this line !!!
	//	if !ok {
	//		fmt.Println(err)
	//		return err
	//	}
	//
	//	for _, value := range temp {
	//		_, ok = Hmap[typeOfS.Field(i).Name][value]
	//		if !ok {
	//			reflect.ValueOf(&structuredParam).Elem().FieldByName(typeOfS.Field(i).Name).SetString("")
	//		}
	//	}
	//}
	//
	//fmt.Println(structuredParam)

	var counter = 0
	var query = "Select * from listing"

	for key, val := range queryParams {
		// Since orderBy has to be in the last
		if key == "orderBy" || key == "sortBy" || key == "limit" {
			continue
		}
		// map the key to a sql map
		if counter < 1 {
			addQuery := fmt.Sprintf(" where ")
			query += addQuery
		} else {
			addQuery := fmt.Sprintf(" and ")
			query += addQuery
		}

		if columnName, ok := min_max_queryParams_map[key]; ok {
			// check if it is for min OR max
			addQuery := fmt.Sprintf("%s %s %s", columnName[0], columnName[1], val[0])
			query += addQuery
		} else {
			query += fmt.Sprintf("%s in (", key)
			length := len(val)

			for ind, tmpValue := range val {
				if ind == length-1 {
					query += fmt.Sprintf(`'%s')`, tmpValue)
				} else {
					query += fmt.Sprintf(`'%s',`, tmpValue)
				}
			}
		}
		counter += 1
	}
	if posted_time, ok := queryParams["posted_time"]; ok {
		addQuery := fmt.Sprintf(" and posted_time<%s", posted_time[0])
		query += addQuery
	}

	// Maybe not perfect can improve this ? (eg: what if more than one order by & map it for asc/desc)
	// we have a soln since we have an array as value for  every key we can do iteration ?

	if orderByvalue, ok := queryParams["orderBy"]; ok {
		// iterate order by & form query
		//for ind, tmp := range orderByvalue {
		//	fmt.Println(tmp, ind)
		//}
		// Only one orderBy is expected
		if sortByvalue, ok := queryParams["sortBy"]; ok {
			addQuery := fmt.Sprintf(" order by %s %s", orderByvalue[0], sortByvalue[0])
			query += addQuery
		} else {
			addQuery := fmt.Sprintf(" order by %s asc", orderByvalue[0])
			query += addQuery
		}

		// For posted_time desc
		addQuery := fmt.Sprintf(", posted_time desc")
		query += addQuery
	} else {
		addQuery := fmt.Sprintf(" order by posted_time desc")
		query += addQuery
	}

	// For pagination
	if limitValue, ok := queryParams["limit"]; ok {
		//if offsetValue, ok := queryParams["offset"]; ok {
		//	addQuery := fmt.Sprintf(" limit %s offset %s", limitValue[0], offsetValue[0]+limitValue[0])
		//	query += addQuery
		//} else {
		//	addQuery := fmt.Sprintf(" limit %s offset 0", limitValue[0])
		//	query += addQuery
		//}
		addQuery := fmt.Sprintf(" limit %s", limitValue[0])
		query += addQuery
	} else {
		addQuery := fmt.Sprintf(" limit 10")
		query += addQuery
	}

	query += ";"
	fmt.Println("Query is:" + query) //

	rows, err := d.db.Query(query)
	if err != nil {
		fmt.Println(err)
		return err
	}

	defer rows.Close()

	var propertyRow data.Listing
	var array []data.Listing
	for rows.Next() {
		err = rows.Scan(&propertyRow.Listing_id,
			&propertyRow.User_id,
			&propertyRow.Room_type,
			&propertyRow.Rent_amount,
			&propertyRow.Rent_currency,
			&propertyRow.Lease_months,
			&propertyRow.Deposit,
			&propertyRow.Gender_environment,
			&propertyRow.Property_type,
			&propertyRow.Cooking_allowed,
			&propertyRow.Nationality,
			&propertyRow.Rent_negotiable,
			&propertyRow.Owner_staying,
			&propertyRow.Pet_allowed,
			&propertyRow.Floor,
			&propertyRow.Availability_date,
			&propertyRow.Square_feet,
			&propertyRow.Pincode,
			&propertyRow.City,
			&propertyRow.Country,
			&propertyRow.District,
			&propertyRow.Street_name,
			&propertyRow.Block,
			&propertyRow.Posted_date,
			&propertyRow.Posted_time)
		if err != nil {
			fmt.Println(err)
			return err
		}
		array = append(array, propertyRow)
	}

	fmt.Println(array)
	return nil
}
