package listing

import "context"

type ListManager interface {
	CreateRoom(
		ctx context.Context,
		user_id string,
		room_type string,
		rent_amount int,
		rent_currency string,
		lease_months int,
		deposit int,
		gender_environment string,
		property_type string,
		cooking_allowed bool,
		nationality string,
		rent_negotiable bool,
		owner_staying bool,
		pet_allowed bool,
		floor string,
		availability_date string, // Note we have used default current_date in sql DB
		square_feet float64,
		pincode int,
		city string,
		country string) (string, error)
}
