package listing

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/Rohan12152001/Renters/managers/listing/data"
	"github.com/Rohan12152001/Renters/managers/listing/db"
	"github.com/Rohan12152001/Renters/utils"
	"github.com/google/uuid"
	"io/ioutil"
	"net/http"
)

var apiKey = "wOlj1xUHjd5b0jssRcbbRLFmFzSrhZvRfJ20hGxhhgo"
var country = "Singapore"

type manager struct {
	listingDb db.ListingDB
}

type dict struct {
	Items []subset `json:"items"`
}

type subset struct {
	Address map[string]string `json:"address"`
}

func New() ListManager {
	return manager{
		listingDb: db.New(),
	}
}

func (m manager) modifyListing(listing *data.Listing) error {
	// extract the code
	postalCode := listing.Pincode

	// make a sample HTTP GET request
	var reqString = fmt.Sprintf("https://geocode.search.hereapi.com/v1/geocode?qq=postalCode=%d,;country=%s&apiKey=%s", postalCode, country, apiKey)
	res, err := http.Get(reqString)

	// check for response error
	if err != nil {
		return err
	}

	// read all response body
	b, _ := ioutil.ReadAll(res.Body)
	var data dict
	err = json.Unmarshal(b, &data)
	if err != nil {
		fmt.Println(err)
		return err
	}

	listing.District = data.Items[0].Address["district"]
	listing.Street_name = data.Items[0].Address["street"]

	// close response body
	res.Body.Close()
	return nil
}

func (m manager) CreateRoom(ctx context.Context, user_id string, room_type string, rent_amount int, rent_currency string, lease_months int, deposit int, gender_environment string, property_type string, cooking_allowed bool, nationality string, rent_negotiable bool, owner_staying bool, pet_allowed bool, floor string, availability_date string, square_feet float64, pincode int, city string, country string) (string, error) {

	// Form the listing data
	listing := data.Listing{
		Listing_id:         uuid.New().String(),
		User_id:            user_id,
		Room_type:          room_type,
		Rent_amount:        rent_amount,
		Rent_currency:      rent_currency,
		Lease_months:       lease_months,
		Deposit:            deposit,
		Gender_environment: gender_environment,
		Property_type:      property_type,
		Cooking_allowed:    cooking_allowed,
		Nationality:        nationality,
		Rent_negotiable:    rent_negotiable,
		Owner_staying:      owner_staying,
		Pet_allowed:        pet_allowed,
		Floor:              floor,
		Availability_date:  availability_date,
		Square_feet:        square_feet,
		Pincode:            pincode,
		City:               city,
		Country:            country,
		District:           "tmp", // This has to be configured on backend
		Street_name:        "tmp", // This has to be configured on backend
		Block:              0,     // Not given in HERE API ?
	}

	user, ok := utils.GetUserFromContext(ctx)
	if !ok {
		fmt.Println("UserObject not found")
	} else {
		fmt.Println("Printing in listManager: ", user)
	}

	// Modify listing for streetName, district, block
	err := m.modifyListing(&listing)
	if err != nil {
		return "", err
	}

	// Pass the listing to DB
	err = m.listingDb.CreateRoom(listing)
	if err != nil {
		return "", err
	}

	return listing.Listing_id, err
}
