package db

import (
	"fmt"
	"github.com/Rohan12152001/Renters/managers/listing/data"
	"github.com/jmoiron/sqlx"
)

type dbClient struct {
	db *sqlx.DB
}

func New() ListingDB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sqlx.Connect("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	return dbClient{
		db: db,
	}
}

func (d dbClient) CreateRoom(listing data.Listing) error {
	query := "Insert into listing (listing_id, user_id, room_type, rent_amount, rent_currency, lease_months, deposit, gender_environment, property_type, cooking_allowed, nationality, rent_negotiable, owner_staying, pet_allowed, floor, availability_date, square_feet, pincode, city, country, district, street_name, block) values (:listing_id, :user_id, :room_type, :rent_amount, :rent_currency, :lease_months, :deposit, :gender_environment, :property_type, :cooking_allowed, :nationality, :rent_negotiable, :owner_staying, :pet_allowed, :floor, :availability_date, :square_feet, :pincode, :city, :country, :district, :street_name, :block)"
	_, err := d.db.NamedExec(query, &listing)
	if err != nil {
		return err
	}
	return nil
}
