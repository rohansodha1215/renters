package db

import "github.com/Rohan12152001/Renters/managers/listing/data"

type ListingDB interface {
	CreateRoom(listing data.Listing) error
}

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "admin"
	dbname   = "Renters"
)
