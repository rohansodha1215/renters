package data

type Listing struct {
	Listing_id         string  `json:"listing___id"`
	User_id            string  `json:"user___id"`
	Room_type          string  `json:"room___type"`
	Rent_amount        int     `json:"rent___amount"`
	Rent_currency      string  `json:"rent___currency"`
	Lease_months       int     `json:"lease___months"`
	Deposit            int     `json:"deposit"`
	Gender_environment string  `json:"gender___environment"`
	Property_type      string  `json:"property___type"`
	Cooking_allowed    bool    `json:"cooking___allowed"`
	Nationality        string  `json:"nationality"`
	Rent_negotiable    bool    `json:"rent___negotiable"`
	Owner_staying      bool    `json:"owner___staying"`
	Pet_allowed        bool    `json:"pet___allowed"`
	Floor              string  `json:"floor"`
	Availability_date  string  `json:"availability___date"`
	Square_feet        float64 `json:"square___feet"`
	Pincode            int     `json:"pincode"`
	City               string  `json:"city"`
	Country            string  `json:"country"`
	District           string  `json:"district"`
	Street_name        string  `json:"street___name"`
	Block              int     `json:"block"`
}
