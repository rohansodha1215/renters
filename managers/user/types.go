package user

import (
	"github.com/Rohan12152001/Renters/managers/user/data"
)

type UserManager interface {
	CreateUser(
		fname,
		lname,
		email,
		phone,
		country,
		password,
		gender,
		description string) (string, error)
	GetUserbyId(id string) (datas.User, error)
	LoginUser(email, password string) (string, int, error)
}
