package datas

type User struct {
	Fname       string `json:"fname"`
	Lname       string `json:"lname"`
	Email       string `json:"email"`
	Phone       string `json:"phone"`
	Country     string `json:"country"`
	Password    string `json:"password"`
	Gender      string `json:"gender"`
	Description string `json:"description"`
	Id          string `json:"id"`
}
