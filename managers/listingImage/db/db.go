package db

import (
	"fmt"
	"github.com/jmoiron/sqlx"
)

type dbClient struct {
	db *sqlx.DB
}

func New() ListingImageDB {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	db, err := sqlx.Connect("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	return dbClient{
		db: db,
	}
}

func (d dbClient) UploadRoomImage(Array [][]string) error {
	// insert into db
	for key, val := range Array {
		fmt.Println(key, val)
		val[0] = "'" + val[0] + "'"
		val[1] = "'" + val[1] + "'"
		query := fmt.Sprintf("Insert into listingImage (listing_id, image_name) values (%s, %s)", val[0], val[1])
		fmt.Println("Query: ", query)
		_, err := d.db.Exec(query)
		if err != nil {
			return err
		}
	}
	return nil
}
