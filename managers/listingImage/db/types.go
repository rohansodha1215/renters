package db

type ListingImageDB interface {
	UploadRoomImage(Array [][]string) error
}

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "admin"
	dbname   = "Renters"
)
