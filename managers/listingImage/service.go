package listingImage

import (
	"fmt"
	"github.com/Rohan12152001/Renters/managers/listingImage/db"
)

type manager struct {
	listingImageDb db.ListingImageDB
}

func New() manager {
	return manager{
		listingImageDb: db.New(),
	}
}

func (m manager) UploadRoomImage(Array [][]string) error {
	// call the function
	err := m.listingImageDb.UploadRoomImage(Array)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}
