
create table listing(
    listing_id varchar(255) primary key,
    user_id varchar(255),
    room_type varchar(50),
    rent_amount int,
    rent_currency varchar(20),
    lease_months int,
    deposit int,
    gender_environment varchar(50),
    property_type varchar(50),
    cooking_allowed boolean,
    nationality varchar(50),
    rent_negotiable boolean,
    owner_staying boolean,
    pet_allowed boolean,
    floor varchar(30),
    availability_date date,
    square_feet decimal,
    pincode int,
    city varchar(50),
    country varchar(50),
    district varchar(50),
    street_name varchar(50),
    block int,
    posted_date date DEFAULT current_date,
    posted_time bigint NOT NULL DEFAULT (date_part('epoch'::text, now()) * (1000)::double precision)
)






