-- For Image table
create table listingImage (
	listing_id varchar(255),
	image_name varchar(255),
	CONSTRAINT fk_listing_id
  	FOREIGN KEY(listing_id)
  	REFERENCES listing(listing_id)
	on delete cascade
);